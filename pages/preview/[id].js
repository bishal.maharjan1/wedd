import React, { useState, useEffect } from "react";
import Head from "next/head";
import { useRouter } from "next/router";

export default function Preview() {
  const router = useRouter();
  const [name, setName] = React.useState("");
  const [ctx, setCtx] = React.useState(null);

  React.useEffect(() => {
    setName(router.query.id);
    if (router.query.id) {
      function addTextToImage(imagePath, text) {
        var i1 = document.getElementById("canvas");
        setCtx(i1);
        var context = i1.getContext("2d");

        // Draw Image function
        var img = new Image();
        img.src = imagePath;
        img.onload = function () {
          context.drawImage(
            img,
            0,
            0,
            img.width,
            img.height, // source rectangle
            0,
            0,
            canvas.width,
            canvas.height
          );
          context.lineWidth = 1;
          context.fillStyle = "yellow";
          context.lineStyle = "#ffff00";
          context.font = "32px Allura";
          context.fillText(text, 50, 587);
        };
      }

      addTextToImage("/images/full.jpg", router.query.id);
    }
  }, [router.query.id]);

  const printDocument = () => {
    var url = ctx.toDataURL("image/png");
    var link = document.createElement("a");
    link.download = "inside.png";
    link.href = url;
    link.click();
  };

  return (
    <div className="container">
      <Head>
        <title>{name}</title>
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Allura&family=Qwigley&display=swap"
          rel="stylesheet"
        ></link>
      </Head>
      <main>
        <div className="n">
          <h1>Invitation for {name}</h1>
          <button className="button" onClick={printDocument}>
            <div className="button__content">
              <p className="button__text">Download</p>
            </div>
          </button>
        </div>
        <div>
          <canvas id="canvas" width="1200" height="700"></canvas>
          {/* <img src="/images/inside.jpg" alt="Inside" height={700} width={600} /> */}
        </div>{" "}
      </main>
      <style jsx>{`
        .n {
          display: flex;
          justify-content: space-between;
          align-items: center;
          width: 100%;
        }
        .button {
          position: relative;
          padding: 0;
          width: 200px;
          height: 75px;
          border: 4px solid #888888;
          outline: none;
          background-color: #f4f5f6;
          border-radius: 40px;
          box-shadow: -6px -20px 35px #ffffff, -6px -10px 15px #ffffff,
            -20px 0px 30px #ffffff, 6px 20px 25px rgba(0, 0, 0, 0.2);
          transition: 0.13s ease-in-out;
          cursor: pointer;
        }
        .button:active {
          box-shadow: none;
        }
        .button:active .button__content {
          box-shadow: none;
        }
        .button:active .button__content .button__text,
        .button:active .button__content .button__icon {
          transform: translate3d(0px, 0px, 0px);
        }
        .button__content {
          position: relative;
          width: 100%;
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          box-shadow: inset 0px -8px 0px #dddddd, 0px -8px 0px #f4f5f6;
          border-radius: 40px;
          transition: 0.13s ease-in-out;
          z-index: 1;
        }
        .button__text {
          position: relative;
          transform: translate3d(0px, -4px, 0px);
          margin: 0;
          text-align: center;
          font-size: 32px;
          background-color: #888888;
          color: transparent;
          text-shadow: 2px 2px 3px rgba(255, 255, 255, 0.5);
          -webkit-background-clip: text;
          -moz-background-clip: text;
          background-clip: text;
          transition: 0.13s ease-in-out;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 2rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
          font-family: "Allura", cursive;
          font-size: 1.6rem;
        }
      `}</style>
    </div>
  );
}

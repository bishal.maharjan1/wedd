import React, { useState } from "react";
import Head from "next/head";
import Link from 'next/link'

export default function Home() {
  const [name, setName] = React.useState("");

  const handleChange = (value) => {
    setName(value);
    console.log(name);
  };

  return (
    <div className="container">
      <Head>
        <title>Wedding Card</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main>
        <h1 className="title">
          Write a <a>Invitation !</a>
        </h1>

        <p className="description">
          <code>Get started by writing</code>
        </p>

        <div className="mainc">
          <div
            style={{
              marginTop: 20,
              marginBottom: 20,
              padding: 20,
              width: "100%",
            }}
          >
            <input
              type="text"
              className="mainc-tx"
              value={name}
              onChange={(e) => handleChange(e.target.value)}
            ></input>
          </div>
          <Link href={`/preview/${name}`}>
            <button className="button">
              <div className="button__content">
                <p className="button__text">Create</p>
              </div>
            </button>
          </Link>
        </div>
      </main>

      <footer>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by Maharjan™
        </a>
      </footer>

      <style jsx>{`
        .mainc {
          display: flex;
          justify-content: center;
          align-items: center;
          flex-direction: column;
          width: 100%;
        }
        .mainc-tx {
          font-size: 42px;
          width: 100%;
          border-radius: 10px;
          text-align: center;
        }
        .button {
          position: relative;
          padding: 0;
          width: 150px;
          height: 75px;
          border: 4px solid #888888;
          outline: none;
          background-color: #f4f5f6;
          border-radius: 40px;
          box-shadow: -6px -20px 35px #ffffff, -6px -10px 15px #ffffff,
            -20px 0px 30px #ffffff, 6px 20px 25px rgba(0, 0, 0, 0.2);
          transition: 0.13s ease-in-out;
          cursor: pointer;
        }
        .button:active {
          box-shadow: none;
        }
        .button:active .button__content {
          box-shadow: none;
        }
        .button:active .button__content .button__text,
        .button:active .button__content .button__icon {
          transform: translate3d(0px, 0px, 0px);
        }
        .button__content {
          position: relative;
          width: 100%;
          height: 100%;
          display: flex;
          justify-content: center;
          align-items: center;
          box-shadow: inset 0px -8px 0px #dddddd, 0px -8px 0px #f4f5f6;
          border-radius: 40px;
          transition: 0.13s ease-in-out;
          z-index: 1;
        }
        .button__text {
          position: relative;
          transform: translate3d(0px, -4px, 0px);
          margin: 0;
          text-align: center;
          font-size: 32px;
          background-color: #888888;
          color: transparent;
          text-shadow: 2px 2px 3px rgba(255, 255, 255, 0.5);
          -webkit-background-clip: text;
          -moz-background-clip: text;
          background-clip: text;
          transition: 0.13s ease-in-out;
        }
        .container {
          min-height: 100vh;
          padding: 0 0.5rem;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        footer {
          width: 100%;
          height: 100px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer img {
          margin-left: 0.5rem;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        a {
          color: inherit;
          text-decoration: none;
        }

        .title a {
          color: #0070f3;
          text-decoration: none;
        }

        .title a:hover,
        .title a:focus,
        .title a:active {
          text-decoration: underline;
        }

        .title {
          margin: 0;
          line-height: 1.15;
          font-size: 4rem;
        }

        .title,
        .description {
          text-align: center;
        }

        .description {
          line-height: 1.5;
          font-size: 1.5rem;
        }

        code {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
          font-size: 1.1rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono,
            DejaVu Sans Mono, Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        .card {
          margin: 1rem;
          flex-basis: 45%;
          padding: 1.5rem;
          text-align: left;
          color: inherit;
          text-decoration: none;
          border: 1px solid #eaeaea;
          border-radius: 10px;
          transition: color 0.15s ease, border-color 0.15s ease;
        }

        .card:hover,
        .card:focus,
        .card:active {
          color: #0070f3;
          border-color: #0070f3;
        }

        .card h3 {
          margin: 0 0 1rem 0;
          font-size: 1.5rem;
        }

        .card p {
          margin: 0;
          font-size: 1.25rem;
          line-height: 1.5;
        }

        .logo {
          height: 1em;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }
      `}</style>

      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: -apple-system, BlinkMacSystemFont, Segoe UI, Roboto,
            Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue,
            sans-serif;
        }

        * {
          box-sizing: border-box;
        }
      `}</style>
    </div>
  );
}
